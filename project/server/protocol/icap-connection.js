
'use strict';

const IcapHandler = require('./icap-handler');
const TcpClient = require('z-abs-stacklayer-core-server/connection/tcp-client');


class IcapConnection {
  constructor(name) {
    this.name = name;
    this.tcpClient = null;
    this.icapHandler = null;
  }
  
  send(requestTarget, httpRequest, httpResponse, cb, cbConnection) {
    this.icapHandler.send(requestTarget, httpRequest, httpResponse, cb, cbConnection);
  }
  
  connect(port) {
    this.tcpClient = new TcpClient((connected, downTicks) => {
      this.icapHandler = new IcapHandler(this.tcpClient, 'www.actorjs.com');
     // console.log(`Connecting[icap]: ${this.name}:`, connected, 'Tick:', downTicks);
    }, (err) => {
      if('ECONNREFUSED' !== err.code) {
        console.log(`Connecting[icap]: ${this.name} - Error:`, err);
      }
    });
    this.tcpClient.start('127.0.0.1', 0, '127.0.0.1', port);
  }
  
  disconnect(connection, done) {
    this.tcpClient.stop(() => {
      this.icapHandler = null;
      this.tcpClient = null;
      done();
    });
  }
}


module.exports = IcapConnection;
