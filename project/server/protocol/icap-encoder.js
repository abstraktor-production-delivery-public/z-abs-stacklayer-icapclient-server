
'use strict';


class IcapEncoder {
  static encode(msg) {
    let sizeObject = IcapEncoder.getSize(msg);
    IcapEncoder.Encapsulated(msg, sizeObject);
    const buffer = Buffer.allocUnsafe(sizeObject.total);
    let offset = 0;
    offset += buffer.write(`${msg.method} ${msg.requestTarget} ${msg.icapVersion}\r\n`, offset);
    offset = IcapEncoder.encodeHeaders(msg, offset, buffer);
    if(msg.hasHttpRequest()) {
      const httpRequest = msg.getHttpRequest();
      offset = IcapEncoder.encodeHttpMsg(httpRequest, offset, buffer, true);
    }
    if(msg.hasHttpResponse()) {
      const httpResponse = msg.getHttpResponse();
      offset = IcapEncoder.encodeHttpMsg(httpResponse, offset, buffer, false);
    }
    return buffer;
  }
  
  static getSize(msg) {
    const sizeObject = {
      total: 0,
      reqHdr: false,
      reqBody: false,
      reqHdrStart: 0,
      reqHdrEnd: 0,
      resHdr: false,
      resBody: false,
      resHdrStart: 0,
      resHdrEnd: 0
    };
    let size = msg.method.length + msg.requestTarget.length + msg.icapVersion.length + 4 + IcapEncoder.getHeadersLength(msg);
    if(msg.hasHttpRequest()) {
      sizeObject.reqHdr = true;
      const httpRequest = msg.getHttpRequest();
      size += httpRequest.method.length + httpRequest.requestTarget.length + httpRequest.httpVersion.length + 4;
      size += IcapEncoder.getHeadersLength(httpRequest);
      if(msg.hasHttpRequestBody()) {
        sizeObject.reqBody = true;
        sizeObject.reqHdrEnd = size;
        const httpRequestBody = httpRequest.getBody();
        size += IcapEncoder.getBodyLength(httpRequestBody);
      }
    }
    if(msg.hasHttpResponse()) {
      sizeObject.resHdr = true;
      sizeObject.resHdrStart = size;
      const httpResponse = msg.getHttpResponse();
      size += httpResponse.httpVersion.length + httpResponse.statusCodeString.length + httpResponse.reasonPhrase.length + 4;
      size += IcapEncoder.getHeadersLength(httpResponse);
      if(msg.hasHttpResponseBody()) {
        sizeObject.resBody = true;
        sizeObject.resHdrEnd = size;
        const httpResponseBody = httpResponse.getBody();
        size += IcapEncoder.getBodyLength(httpResponseBody);
      }
    }
    sizeObject.total = size;
    return sizeObject;
  }
  
  static getHeadersLength(msg) {
    let length = 0;
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        length += name.length + value.length + 4;
      });
    });
    length += 2;
    return length;
  }
  
  static getBodyLength(body) {
    let size = body.length;
    const chunkHeader = size.toString(16);
    size += chunkHeader.length + 2 + 2 + 5;
    return size;
  }
  
  static Encapsulated(msg, sizeObject) {
    let encapuladed = '';
    if(sizeObject.reqHdr) {
      encapuladed += `req-hdr=${sizeObject.reqHdrStart}`;
      if(!sizeObject.reqBody) {
        if(!sizeObject.resBody) {
          encapuladed += `, null-body=${sizeObject.reqHdrEnd}`;
        }
      }
      else {
        encapuladed += `, req-body=${sizeObject.reqHdrEnd}`;
      }
    }
    if(sizeObject.resHdr) {
      encapuladed += `${encapuladed ? ', ' : ''}res-hdr=${sizeObject.resHdrStart}`;
      if(!sizeObject.resBody) {
        encapuladed += `, null-body=${sizeObject.resHdrEnd}`;
      }
      else {
        encapuladed += `, res-body=${sizeObject.resHdrEnd}`;
      }
    }
    if(msg) {
      msg.addHeader('Encapsulated', encapuladed);
    }
    sizeObject.total += 'Encapsulated'.length + 2 + encapuladed.length + 2;
  }
  
  static encodeHeaders(msg, offset, buffer) {
    let s = '';
    const oldoffset = offset;
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        s += name;
        offset += buffer.write(name, offset);
        s += ': ';
        offset += buffer.write(': ', offset);
        s += value;
        offset += buffer.write(value, offset);
        s += '\r\n';
        offset += buffer.write('\r\n', offset);
      });
    });
    s += '\r\n';
    offset += buffer.write('\r\n', offset);
    return offset;
  }
  
  static encodeHttpMsg(msg, offset, buffer, request) {
    const oldoffset = offset;
    if(request) {
      offset += buffer.write(`${msg.method} ${msg.requestTarget} ${msg.httpVersion}\r\n`, offset);
    }
    else {
      offset += buffer.write(`${msg.httpVersion} ${msg.statusCode} ${msg.reasonPhrase}\r\n`, offset);
    }
    offset = IcapEncoder.encodeHeaders(msg, offset, buffer);      
    if(msg.hasBody()) {
      const body = msg.getBody();
      const chunkHeader = body.length.toString(16);
      offset += buffer.write(`${chunkHeader}\r\n`, offset);
      offset += body.copy(buffer, offset);
      offset += buffer.write('\r\n0\r\n\r\n', offset);
    }
    return offset;
  }
}


module.exports = IcapEncoder;
