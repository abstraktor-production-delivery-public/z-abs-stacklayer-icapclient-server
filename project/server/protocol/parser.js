
'use strict';

const IcapMsgRequest = require('../msg/icap-msg-request');
const IcapMsgResponse = require('../msg/icap-msg-response');
const HttpMsgRequest = require('z-abs-stacklayer-httpclient-server/msg/http-msg-request');
const HttpMsgResponse = require('z-abs-stacklayer-httpclient-server/msg/http-msg-response');
const BufferManager = require('z-abs-stacklayer-core-server/connection/buffer-manager');


class Parser {
  constructor(id, type, replaceHeaderNames = new Map()) {
    this.id = id;
    this.currentId = ++Parser.CURRENT_ID;
    this.type = type;
    this.replaceHeaderNames = replaceHeaderNames;
    this.bufferManager = new BufferManager(this.id, true);
    this.state = Parser.NONE;
    this.chunks = null;
    this.message = null;
    this.msgHttpRequest = null;
    this.msgHttpResponse = null;
    this.rawBody = [];
    this.requestData = null;
    this.responseData = null;
  }
  
  parse(chunk) {
    this.bufferManager.receiveBuffer(chunk);
    switch(this.state) {
      case Parser.NONE: {
        if(!this.bufferManager.findLine()) {
          return Parser.RESULT_NONE;
        }
        else {
          this.rawBody = [];
          this.chunks = null;
          this.msgHttpRequest = null;
          this.msgHttpResponse = null;
          const line = this.bufferManager.getLine();
          if(Parser.REQUEST === this.type) {
            this.message = new IcapMsgRequest();
            this.message.addRequestLine(...line.split(' '));
          }
          else {
            this.message = new IcapMsgResponse();
            this.message.addStatusLine(...line.split(' '));
          }
          this._setState(Parser.HEADERS);
        }
      }
      case Parser.HEADERS: {
        let allHeaders = false;
        while(this.bufferManager.findLine()) {
          const line = this.bufferManager.getLine();
          if(0 === line.length) {
            this._setState(Parser.BODY);
            allHeaders = true;
            break;
          }
          const delimiter = line.indexOf(':');
          if(-1 === delimiter) {
            console.log('parse Error:', delimiter);
          }
          this.message.addHeader(line.substring(0, delimiter), line.substring(delimiter + 1).trimStart());
        }
        if(!allHeaders) {
          return Parser.RESULT_NONE;
        }
       
        const encapsulated = this.message.getHeader('Encapsulated');
        if(encapsulated) {
          const encapsulatedDatas = encapsulated.split(', ');
          if(0 === encapsulatedDatas.length) {
            return Parser.RESULT_BODY_ALL;
          }
          const encapsulatedObjects = [];
          encapsulatedDatas.forEach((encapsulatedData) => {
            const data = encapsulatedData.split('=');
            encapsulatedObjects.push({
              name: data[0],
              start: data[1]
            });
          });
          this.requestData = this._getRequest(encapsulatedObjects);
          this.responseData = this._getResponse(encapsulatedObjects);    
          this._setState(Parser.BODY_HTTP_REQUEST_FIRST_LINE);
        }
      }
      case Parser.BODY_HTTP_REQUEST_FIRST_LINE: {
        if(this.requestData) {
          if(!this.bufferManager.findLine()) {
            return Parser.RESULT_NONE;
          }
          else {
            this.rawBody = [];
            this.chunks = null;
            const line = this.bufferManager.getLine();
            this.msgHttpRequest = new HttpMsgRequest();
            this.message.addHttpRequest(this.msgHttpRequest);
            this.msgHttpRequest.addRequestLine(...line.split(' '));
          }
        }
        this._setState(Parser.BODY_HTTP_REQUEST_HEADERS);
      }
      case Parser.BODY_HTTP_REQUEST_HEADERS: {
        if(this.requestData) {
          let allHeaders = false;
          while(this.bufferManager.findLine()) {
            const line = this.bufferManager.getLine();
            if(0 === line.length) {
              this._setState(Parser.BODY_HTTP_REQUEST_BODY);
              allHeaders = true;
              break;
            }
            const delimiter = line.indexOf(':');
            if(-1 === delimiter) {
              console.log('parse Error:', delimiter);
            }
            this.msgHttpRequest.addHeader(line.substring(0, delimiter), line.substring(delimiter + 1).trimStart());
          }
          if(!allHeaders) {
            return Parser.RESULT_NONE;
          }
        }
      }
      case Parser.BODY_HTTP_REQUEST_BODY: {
        if(this.requestData && -1 !== this.requestData.bodyStart) {
        }
        this._setState(Parser.BODY_HTTP_RESPONSE_FIRST_LINE);
      }
      case Parser.BODY_HTTP_RESPONSE_FIRST_LINE: {
        if(this.responseData) {
          if(!this.bufferManager.findLine()) {
            return Parser.RESULT_NONE;
          }
          else {
            this.rawBody = [];
            this.chunks = null;
            const line = this.bufferManager.getLine();
            this.msgHttpResponse = new HttpMsgResponse();
            this.message.addHttpResponse(this.msgHttpResponse);
            this.msgHttpResponse.addStatusLine(...line.split(' '));
          }
        }
        this._setState(Parser.BODY_HTTP_RESPONSE_HEADERS);
      }
      case Parser.BODY_HTTP_RESPONSE_HEADERS: {
        if(this.responseData) {
          let allHeaders = false;
          while(this.bufferManager.findLine()) {
            const line = this.bufferManager.getLine();
            if(0 === line.length) {
              this._setState(Parser.BODY_HTTP_RESPONSE_BODY);
              allHeaders = true;
              break;
            }
            const delimiter = line.indexOf(':');
            if(-1 === delimiter) {
              console.log('parse Error:', delimiter);
            }
            this.msgHttpResponse.addHeader(line.substring(0, delimiter), line.substring(delimiter + 1).trimStart());
          }
          if(!allHeaders) {
            return Parser.RESULT_NONE;
          }
        }
      }
      case Parser.BODY_HTTP_RESPONSE_BODY: {
        if(this.responseData && -1 !== this.responseData.bodyStart) {
          return this._parseTransferEncoding(this.msgHttpResponse);
        }
      }
      default: {
        
      }
    }
    return false;
  }
  
  _parseTransferEncoding(msg) {
    if(null === this.chunks) {
      this.chunks = {
        state: Parser.TE_SIZE,
        size: 0,
        chunkLines: [],
        chunks: [],
        trailers: []
      };
    }
    
    switch(this.chunks.state) {
      case Parser.TE_SIZE:
      case Parser.TE_DATA:
      case Parser.TE_DATA_LINE: {
        // 1) Get Data
        while(true) {
          if(Parser.TE_SIZE === this.chunks.state) {
            if(!this.bufferManager.findLine()) {
              return false;
            }
            this.chunks.state = Parser.TE_DATA;
            const line = this.bufferManager.getLine();
            this.chunks.chunkLines.push(line);
            const chunkLineIndex = line.indexOf(Parser.COMMA_SP);
            if(-1 === chunkLineIndex) {
              this.chunks.size = Number.parseInt(line, 16);
            }
            else {
              this.chunks.size = Number.parseInt(line.substring(0, chunkLineIndex), 16);
            }
          }
          if(Parser.TE_DATA === this.chunks.state) {
            if(0 !== this.chunks.size) {
              if(Number.isNaN(this.chunks.size)) {
                return true; // THROW ?
              }
              const buffer = this.bufferManager.receiveSize(this.chunks.size);
              if(undefined !== buffer) {
                msg.addBody(buffer);
                this.chunks.chunks.push(buffer);
                this.chunks.size = 0;
                this.chunks.state = Parser.TE_DATA_LINE;
              }
              else {
                return false;
              }
            }
            else {
              this.chunks.state = Parser.TE_TRAILER;
              break;
            }
          }
          if(Parser.TE_DATA_LINE === this.chunks.state) {
            if(!this.bufferManager.findLine()) {
              return false;
            }
            const line = this.bufferManager.getLine();
            this.chunks.state = Parser.TE_SIZE;
          }
        }
        break;
      }
    };
    switch(this.chunks.state) {
      case Parser.TE_TRAILER: {
        // 2) Get Trailers
        while(true) {
          if(!this.bufferManager.findLine()) {
            return Parser.RESULT_HEADERS;
          }
          const line = this.bufferManager.getLine();
          if(0 === line.length) {
            this.chunks.state = Parser.TE_DONE;
            break;
          }
          this.chunks.trailers.push(line);
        }
      }
    };
    this._setState(Parser.NONE);
    return true;
  }
  
  _getRequest(encapsulatedObjects) {
    const o1 = encapsulatedObjects[0];
    if(!o1 || 'req-hdr' !== o1.name) {
      return null;
    }
    else {
      const request = {
        headerStart: o1.start,
        headerEnd: -1,
        bodyStart: -1,
        bodyEnd: -1
      };
      const o2 = encapsulatedObjects[1];
      if(!o2) {
        console.log('ERROR');
        return null;
      }
      if('null-body' !== o2.name || 'res-hdr' !== o2.name) {
        request.headerEnd = o2.start;
      }
      else if('res-body' !== o2.name) {
        request.headerEnd = o2.start;
        request.bodyStart = o2.start;
      }
      else {
        console.log('ERROR');
      }
      return request;
    }
  }
  
  _getResponse(encapsulatedObjects) {
    let foundIndex = -1;
    for(let i = 0; i < encapsulatedObjects.length; ++i) {
      if('res-hdr' === encapsulatedObjects[i].name) {
        foundIndex = i;
        break;
      }
    }
    if(-1 === foundIndex) {
      return null;
    }
    const response = {
      headerStart: encapsulatedObjects[foundIndex].start,
      headerEnd: -1,
      bodyStart: -1,
      bodyEnd: -1
    };
    const o2 = encapsulatedObjects[foundIndex + 1];
    if(!o2) {
      console.log('ERROR');
      return null;
    }
    if('null-body' === o2.name) {
      response.headerEnd = o2.start;
    }
    else if('res-body' === o2.name) {
      response.headerEnd = o2.start;
      response.bodyStart = o2.start;
    }
    return response;
  }
  
  isHeadersReady() {
    return this.state > Parser.HEADERS;
  }
  
  _setState(state) {
    this.state = state;
  }
}

Parser.CR_CL = '\r\n';
Parser.CR_CL_LENGTH = Parser.CR_CL.length;
Parser.COMMA_SP = ', ';


Parser.RESULT_NONE = 0;
Parser.RESULT_HEADERS = 1;
Parser.RESULT_BODY_PART = 2;
Parser.RESULT_BODY_ALL = 2;

Parser.REQUEST = 0;
Parser.RESPONSE = 1;

Parser.NONE = 0;
Parser.HEADERS = 1;
Parser.BODY_HTTP_REQUEST_FIRST_LINE = 2;
Parser.BODY_HTTP_REQUEST_HEADERS = 3;
Parser.BODY_HTTP_REQUEST_BODY = 4;
Parser.BODY_HTTP_RESPONSE_FIRST_LINE = 5;
Parser.BODY_HTTP_RESPONSE_HEADERS = 6;
Parser.BODY_HTTP_RESPONSE_BODY = 7;

Parser.STATUS = [
  'NONE',
  'HEADERS',
  'BODY_HTTP_REQUEST_FIRST_LINE',
  'BODY_HTTP_REQUEST_HEADERS',
  'BODY_HTTP_REQUEST_BODY',
  'BODY_HTTP_RESPONSE_FIRST_LINE',
  'BODY_HTTP_RESPONSE_HEADERS',
  'BODY_HTTP_RESPONSE_BODY'
];

Parser.TE_SIZE = 0;
Parser.TE_DATA = 1;
Parser.TE_DATA_LINE = 2;
Parser.TE_TRAILER = 3;
Parser.TE_DONE = 4;

Parser.TE_STATUS = [
  'TE_SIZE',
  'TE_DATA',
  'TE_DATA_LINE',
  'TE_TRAILER',
  'TE_DONE'
];

Parser.CURRENT_ID = 0;


module.exports = Parser;
