
'use strict';

const IcapRespmod = require('../msg/icap-respmod');
const IcapEncoder = require('./icap-encoder');
const IcapParser = require('./parser');


class IcapHandler {
  constructor(tcpClient, host) {
    this.tcpClient = tcpClient;
    this.host = host;
    this.parser = new IcapParser(++IcapHandler.id, IcapParser.RESPONSE);
  }
  
  send(requestTarget, httpRequest, httpResponse, cb, cbConnection) {
    this.tcpClient.send(IcapEncoder.encode(new IcapRespmod(requestTarget, 'www.actorjs.com', httpRequest, httpResponse)), (data) => {
      if(this.parser.parse(data)) {
        const icapMsg = this.parser.message;
        const statusCode = icapMsg.statusCode;
        cb(statusCode, icapMsg);
        return true;
      }
      else {
        return false;
      }
    }, () => {}, cbConnection);
  }
}

IcapHandler.id = 0;

module.exports = IcapHandler;
