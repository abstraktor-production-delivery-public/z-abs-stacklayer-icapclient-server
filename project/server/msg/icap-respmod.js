
'use strict';

const IcapRespmodRequest = require('./icap-msg-request');


class IcapRespmod extends IcapRespmodRequest {
  constructor(requestTarget, host, httpRequest, httpResponse) {
    super('RESPMOD', requestTarget, 'ICAP/1.0');
    this.addHeader('Host', host);
    if(httpRequest) {
      this.addHttpRequest(httpRequest);
    }
    if(httpResponse) {
      this.addHttpResponse(httpResponse);
    }
  }
}

module.exports = IcapRespmod;
